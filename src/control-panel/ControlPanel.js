import React from 'react'
import './ControlPanel.css'
import { types } from '../constants'

const typesBtn = [
  {
    id: 'qweqwe',
    name: 'B',
    eventType: types.bold,
  },
  {
    id: 'adaff',
    name: 'I',
    eventType: types.italic,
  },
  {
    id: 'vdewrv',
    name: 'U',
    eventType: types.underline,
  },
]

const ControlPanel = ({clickEvent, activeMarks}) => {
  const activeMark = (type) => {
    const hasMark = activeMarks.some(mark => mark.type === type);
    return hasMark ? 'is-active' : ''
  }

  const renderBtn = () => (
    typesBtn.map(item => (
      <button key={item.id}
              className={`format-action ${activeMark(item.eventType)}`}
              type="button"
              onPointerDown={clickEvent(item.eventType)}>
        <b>{item.name}</b>
      </button>
    ))
  )

  return (
    <div id="control-panel">
      <div id="format-actions">
        {renderBtn()}
      </div>
    </div>
  )
}

export default ControlPanel
