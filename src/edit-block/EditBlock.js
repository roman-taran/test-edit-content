import React, { Component, Fragment } from 'react'
import ControlPanel from '../control-panel/ControlPanel'
import { Editor } from 'slate-react'
import { Value } from 'slate'
import BoldMark from '../bold-mark/BoldMark'
import ItalicMark from '../italic-mark/ItalicMark'
import getMockText from '../text.service'
import { types } from '../constants'
import './EditBlock.css'

const initValue = (value = '') => (
  Value.fromJSON({
    document: {
      nodes: [
        {
          object: 'block',
          type: 'paragraph',
          nodes: [
            {
              object: 'text',
              leaves: [
                {
                  text: value,
                },
              ],
            },
          ],
        },
      ],
    },
  })
)

class EditBlock extends Component {
  state = {
    value: initValue(),
  }

  componentDidMount () {
    this.setState({loading: true})
    getMockText()
      .then((res) => {
        this.setState({value: initValue(res)})
      })
      .catch((error) => {
        throw Error
      })
  }

  onChange = ({value}) => {
    this.setState({value})
  }

  onKeyDown = (e, change) => {
    if (!e.ctrlKey) return
    e.preventDefault()

    switch (e.key) {
      case 'b': {
        change.toggleMark(types.bold)
        return true
      }
      case 'i': {
        change.toggleMark(types.italic)
        return true
      }

      case 'u': {
        change.toggleMark(types.underline)
        return true
      }
      default: {
        return
      }
    }
  }

  renderMark = (props) => {
    switch (props.mark.type) {
      case types.bold:
        return <BoldMark {...props} />

      case types.italic:
        return <ItalicMark {...props} />

      case types.underline:
        return <u {...props.attributes}>{props.children}</u>

      default: {
        return
      }
    }
  }

  onMarkClick = (type) => (e) => {
    e.preventDefault()
    const newChange = this.editor.toggleMark(type)
    this.onChange(newChange)
  }

  ref = editor => {
    this.editor = editor
  }

  render () {
    const {value} = this.state
    return (
      <Fragment>
        <ControlPanel clickEvent={this.onMarkClick}
                      activeMarks={value.activeMarks}
        />
        <div id='file-zone'>
          <div id="file">
            <Editor ref={this.ref}
                    value={value}
                    onChange={this.onChange}
                    onKeyDown={this.onKeyDown}
                    renderMark={this.renderMark}
            />
          </div>
        </div>
      </Fragment>
    )
  }

}

export default EditBlock
