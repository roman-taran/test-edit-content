import React from 'react'
import './App.css'
import EditBlock from './edit-block/EditBlock'


const App = () => {
		return (
      <div className="App">
        <header>
          <span>Simple Text Editor</span>
        </header>
        <main>
          <EditBlock/>
        </main>
      </div>
    )
}

export default App
